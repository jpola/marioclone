﻿using UnityEngine;
using System.Collections;

public class GumbasController : MonoBehaviour {

    private Rigidbody2D m_RigidBody2D;
    private Animator m_Animator;
    private BoxCollider2D m_BoxCollider2D;
    private SpriteRenderer m_SpriteRenderer;
    [SerializeField]
    private Sprite m_DeadGumbas;

    [SerializeField]
    private Vector2 m_Velocity;


    private bool m_isAlive;



    void Awake () {

        m_RigidBody2D = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_BoxCollider2D = GetComponent<BoxCollider2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();




        m_isAlive = true;

    }
	// Use this for initialization
	void Start () {

        m_RigidBody2D.velocity = m_Velocity;
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (m_isAlive) {
            m_RigidBody2D.velocity = new Vector2(m_Velocity.x, m_RigidBody2D.velocity.y);
        }
        else {
            m_RigidBody2D.velocity = Vector2.zero;
        }
	}

    void FlipVelocity () {
        m_Velocity = new Vector2(-1 * m_Velocity.x, m_Velocity.y);
    }
    void OnCollisionEnter2D (Collision2D c) {
        if (c.collider.tag == "Wall") {
            FlipVelocity();
        }

        if (c.collider.tag == "Player") {
            //
            Transform groundCheck = c.collider.transform.FindChild("GroundCheck");
            if (groundCheck == null) {
                Debug.LogError("Problem with getting groundCheck from Player");
            }

            if (groundCheck.position.y < transform.position.y + 0.8f) {
                Debug.Log("Mario should die");
                FlipVelocity();
            }
            else {
                Debug.Log("Mario should kill gumbass");
                m_Animator.enabled = false;
                m_Velocity = Vector2.zero;
                m_SpriteRenderer.sprite = m_DeadGumbas;
                m_BoxCollider2D.size = new Vector2(m_BoxCollider2D.size.x, 0.4f);

                Physics2D.IgnoreCollision(c.collider, m_BoxCollider2D);

                Rigidbody2D m_rb = c.collider.GetComponent<Rigidbody2D>();
                Vector2 m_cv = m_rb.velocity;

                m_rb.velocity = new Vector2(m_cv.x, 15f);

                Destroy(gameObject, 1.25f);
                



            }

        }
    }
}
