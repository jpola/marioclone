﻿using UnityEngine;
using System.Collections;

public class CoinManager : MonoBehaviour {

	void OnTriggerEnter2D ( Collider2D c) {
        
        if (c.tag == "Player") {
            MarioController mc = c.GetComponent<MarioController>();
            if (mc != null) {
                mc.PickCoin();
                Debug.Log("Coin picked");
                Destroy(gameObject);

            }
        }
    }
}
